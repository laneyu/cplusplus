/*
http://www.cnblogs.com/morewindows/archive/2011/08/26/2154198.html

*/
#include <stdio.h>

//不能通过类名调用非静态成员函数
//静态成员函数中不能引用非静态成员
//因为静态成员属于整个类，在类实例化对象之前就已经分配空间了，
//而类的非静态成员必须在类实例化之后才有内存空间，就好像没有声明一个变量却提前使用它一样
class Point
{
public:
    Point()
        {
            ++m_;
        }
    ~Point()
        {
            --m_;
        }

    static void output()
    {
        printf("%d\n", m_);
    }

private:
    static int m_;
};

int Point::m_ = 0;
int main()
{
    Point pt;
    pt.output();
}
