#include <iostream>
#include <vector>

using namespace std;

int main()
{
    vector<int> ivector(10);

    for (int i = 0; i < ivector.size(); i++)
    {
        ivector[i] = i;
    }

    for (int i = 0; i < ivector.size(); i++)
    {
        cout << ivector[i] << " ";
    }
    cout << endl;

    return 0;
}
