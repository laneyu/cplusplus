#include <iostream>

using namespace std;

class noncopyable
{
protected:
    noncopyable()   //protected 不允许此类进行实例化
        {
            cout << "constructor" <<endl;
        }
    ~noncopyable()
        {
            cout << "destructor" <<endl;
        }

private:

    noncopyable(const noncopyable &a)    //private不允许此类及子类进行拷贝构造操作
        {
            cout <<"copy constructor" <<endl;
        }


    noncopyable& operator=(const noncopyable &a)    //private不允许此类及子类进行赋值操作
        {
            cout <<"= override" <<endl;
        }
    
};


class ClassA :  noncopyable
{
};

void test_noncopyable()
{
 //   noncopyable a;          //构造函数
 //   noncopyable b(a);      //拷贝构造函数
 //   noncopyable d = a;    //拷贝构造函数
 //   noncopyable c;          //构造函数
//    c = a;                      //赋值操作
        
    cout << endl;
}

void test_ClassA()
{
    ClassA a;
//    ClassA b(a);
//    ClassA d = a;
    ClassA c;

//    c = a;
    cout << endl;
}



int main(int argc, char *argv[])
{
    test_noncopyable();
    cout << endl;
    test_ClassA();
}
