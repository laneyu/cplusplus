#include <stdio.h>
#include <string.h>

const int MAX_NAME_SIZE = 30;

class Student
{
public:
    Student(char *name)
    {
        strcpy(this->name_, name);
        ++this->number_;
        this->next_ = head_;
        this->prev_ = NULL;
        if (head_ != NULL)
            head_->prev_ = this;
        head_ = this;
    }
    ~Student()
    {
        if (this == head_)
        {
            head_ = this->next_;
            printf("11\n");
        }
        else
        {
            printf("22\n");
            this->next_->prev_ = this->prev_;
            this->prev_->next_ = this->next_;
        }

        --this->number_;
    }

    static void PrintAllStudents();

private:
    char name_[MAX_NAME_SIZE];
    Student *next_;
    Student *prev_;
    static Student *head_;
    static int number_;
};

void Student::PrintAllStudents()
{
    Student *p = NULL;
    
    for (p = head_; p != NULL; p=p->next_)
        printf("%s\n", p->name_);
    printf("number: %d\n", number_);
}

Student* Student::head_ = NULL;
int Student::number_ = 0;

int main()
{
       Student studentA("AAA");
       Student studentB("BBB");
       Student studentC("CCC");
       Student studentD("DDD");
       
       Student student("MoreWindows");
 //      Student *studentE = new Student("EEE");    //�����ڴ��쳣����
       Student::PrintAllStudents();
}
