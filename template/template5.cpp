//class templates specialization

#include <iostream>

//class template:
template <typename T>
class mycontainer
{
public:
    mycontainer(T element)
        : element_(element)
        {}
    T increase()
    {
        return ++element_;
    }

    void output()
    {
        std::cout <<element_<<std::endl;
    }
private:
    T element_;
};

int main()
{
    mycontainer<int> a(10);
    a.output();
    mycontainer<int> b = a.increase();
    a.output();
    b.output();

    mycontainer<char> a1('A');
    a1.increase();
    a1.output();
}
