//class templates specialization

#include <iostream>

//class template:
template <typename T>
class mycontainer
{
public:
    mycontainer(T element)
        : element_(element)
        {}
    T increase()
    {
        return ++element_;
    }

    void output()
    {
        std::cout <<element_<<std::endl;
    }
private:
    T element_;
};

//class template specialization
template <>
class mycontainer <char>
{
public:
        mycontainer(char element)
            : element_(element)
            {}
        char uppercase()
        {
            if ((element_>='a')&&(element_<='z'))
                element_+='A'-'a';
            return element_;
        }

        char lowercase()
        {
            if ((element_>='A')&&(element_<='Z'))
                element_-='A'-'a';
            return element_;
        }
    
        void output()
        {
            std::cout <<element_<<std::endl;
        }

private:
    char element_;
};
int main()
{
    mycontainer<int> a(10);
    a.output();
    mycontainer<int> b = a.increase();
    a.output();
    b.output();

    mycontainer<char> a1('A');
    a1.lowercase();
    a1.output();
}
