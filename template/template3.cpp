//class templates
//member function is defined outside the defintion of the class template, eg getMax(), shall process with the tmeplate <...> prefix

#include <iostream>

template <typename T>
class mypair
{
public:
    mypair(T first, T second)
    {
        values_[0] = first;
        values_[1] = second;
    }

    T getMax();

    void output()
    {
        std::cout <<values_[0] <<" " <<values_[1] <<std::endl;
    }
private:
    T values_[2];
};

template <typename T>
T mypair<T>::getMax()   //和类区别，如int mypair::getMax()
{
    T retval;
    retval = values_[0] > values_[1] ? values_[0] : values_[1];
    return retval;
}

int main()
{
    mypair<int> myint(115, 36);
    //mypair myint(115, 36);    //template需要一个类型参数
    //template2.cpp:19:12: error: missing template arguments before ‘myint’
    //template2.cpp:19:12: error: expected ‘;’ before ‘myint’

    myint.output();
    
    mypair<double> mydouble(123.5, 1234.6);
    mydouble.output();

    std::cout << myint.getMax() <<std::endl;
}
