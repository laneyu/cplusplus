//class templates

#include <iostream>

template <typename T, typename T2>
class mypair
{
public:
    mypair(T first, T second, T2 z)
    {
        values_[0] = first;
        values_[1] = second;
        z_ = z;
    }

    void output()
    {
        std::cout <<values_[0] <<" " <<values_[1]  <<" " <<z_ <<std::endl;
    }
private:
    T values_[2];
    T2 z_;
};

int main()
{
    mypair<int, char> myint(115, 36, 'A');
    //mypair myint(115, 36);    //template需要一个类型参数
    //template2.cpp:19:12: error: missing template arguments before ‘myint’
    //template2.cpp:19:12: error: expected ‘;’ before ‘myint’

    myint.output();
    
    mypair<double, int> mydouble(123.5, 1234.6, 100);
    mydouble.output();
}
