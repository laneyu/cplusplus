//class templates

#include <iostream>

template <typename T>
class mypair
{
public:
    mypair(T first, T second)
    {
        values_[0] = first;
        values_[1] = second;
    }

    void output()
    {
        std::cout <<values_[0] <<" " <<values_[1] <<std::endl;
    }
private:
    T values_[2];
};

int main()
{
    mypair<int> myint(115, 36);
    //mypair myint(115, 36);    //template需要一个类型参数
    //template2.cpp:19:12: error: missing template arguments before ‘myint’
    //template2.cpp:19:12: error: expected ‘;’ before ‘myint’

    myint.output();
    
    mypair<double> mydouble(123.5, 1234.6);
    mydouble.output();
}
