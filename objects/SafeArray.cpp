#include "SafeArray.h"

#include <iostream>

using namespace std;

#if 0
SafeArray::SafeArray(int len)
{
    length = len;
    _array = new int[length];
    cout << "enter"<<endl;
}
#else
//1 使用成员初始化列表，多个用逗号分开
SafeArray::SafeArray(int len) : length(len)
{
    _array = new int[length];
    cout << "enter"<<endl;
}
#endif

bool SafeArray::isSafe(int i)
{
    if (i >= length || i < 0)
        return false;
    else
        return true;
}

int SafeArray::get(int i)
{
    if (isSafe(i))
        return _array[i];

    return 0;
}

void SafeArray::set(int i, int value)
{
    if (isSafe(i))
        _array[i] = value;
}

//1没有定义destructor则程序会自动建立一个没有内容的destructor
SafeArray::~SafeArray()
{
    delete [] _array;
    cout << "leave"<<endl;
}
