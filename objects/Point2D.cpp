#include <iostream>

#include "Point2D.h"

using namespace std;

Point2D::Point2D()
{
    _x = 0;
    _y = 0;
}

Point2D::Point2D(int x, int y)
{
    _x = x;
    _y = y;
    
}

Point2D Point2D::operator+(const Point2D &p)
{
    Point2D tmp(_x + p._x, _y + p._y);

    return tmp;
}

Point2D Point2D::operator-(const Point2D &p)
{
    Point2D tmp(_x - p._x, _y - p._y);

    return tmp;
}

Point2D& Point2D::operator++()
{
    _x++;
    _y++;

    return *this;
}

Point2D Point2D::operator++(int)
{
    Point2D tmp(_x, _y); 
    _x++; 
    _y++; 

    cout << tmp.x() << " " <<tmp.y()<<endl;

    return tmp; 
}

Point2D& Point2D::operator--() 
{ 
    _x--;   //先处理，然后返回
    _y--; 

    return *this; 
} 

Point2D Point2D::operator--(int) 
{
    Point2D tmp(_x, _y);    //先保存，然后处理
    _x--; 
    _y--; 

    return tmp; 
} 

#if 0
Point2D Point2D::operator+(const Point2D &p, int i)
{
    Point2D tmp(p._x + i, p._y + i);

    return tmp
}

Point2D Point2D::operator+(int i, const Point2D &p)
{
    Point2D tmp(i + p._x, i + p._y);

    return tmp
}
#endif