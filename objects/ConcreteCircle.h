#ifndef __CONCRETE_CIRCLE_H__
#define __CONCRETE_CIRCLE_H__

#include <iostream>
#include "AbstractCircle.h"

using namespace std;

class ConcreteCircle : public AbstractCircle
{
public:
    void render()
        {
        cout << "画一个半径"
               << _radius
               << " 的实心圆"
               << endl;
        }
};

#endif

