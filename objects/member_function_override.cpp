#include <iostream>

using namespace std;

class Foo1
{
public:
    void show()
        {
            cout << "Foo1's show()" <<endl;
        }
};

class Foo2 : public Foo1
{
public:
    void show()
        {
            this->Foo1::show();
            cout << "Foo2's show()"<<endl;
        }
};

int main()
{
    Foo1 f1; 
    Foo2 f2; 

    f1.show(); 
    f2.show(); 
 
    return 0;
}