#include <iostream> 
#include "SafeArray.h"
using namespace std; 

/*
    constructor (构造函数) 实例化的时候调用构造函数
    destructor(析构函数)实例生命周期结束的时候调用
*/

void test()
{
      SafeArray safeArray(10);
 
    for(int i = 0; i < safeArray.length; i++) {
        safeArray.set(i, (i + 1) * 10);
    }
 
    for(int i = 0; i < safeArray.length; i++) {
        cout << safeArray.get(i) << " ";
    }
    cout << endl;
}

int main() {
   test();

    return 0;
}
