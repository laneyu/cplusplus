#ifndef __CUBIC_H__
#define __CUBIC_H__

#include "Rectangle.h"

#include <iostream>

using namespace std;

/*

private 只可以在父类中使用，子类要使用必须使用父类提供的public接口
protected 可以在子类中直接使用，不能够在实例中使用

*/

class Cubic : public Rectangle
{
public:
    Cubic() 
        {
            _z = 0;
            _length = 0;
        }

    Cubic(int x, int y, int z, int length, int width, int height)
        : Rectangle(x, y, width, height), _z(z), _length(length)
        {

        }

    int length() {return _length;}
    int volumn() {return _length*_width*_height;}

protected:
    int _z;
    int _length;
};

#endif

