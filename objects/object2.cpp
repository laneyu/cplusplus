#include <iostream>
#include <string>

using namespace std;

class Ball
{
public:
    Ball(int = 10);
    int getValue();
    void setValue(int);

private:
    int _radius;
};

Ball::Ball(int radius)
{
    _radius = radius;
}

int Ball::getValue()
{
    return _radius;
}

void Ball::setValue(int radius)
{
    _radius = radius;
}

int main()
{
    Ball ball1;
    cout << "ball1" << "\t" << ball1.getValue()<<endl;

    Ball ball2 = 5;
    cout << "ball2" << "\t" << ball1.getValue()<<endl;
}