#include <iostream>

using namespace std;

/*
showFooByPtr()與showFooByRef()函式並無法事先知道要操作的是哪一個物件的哪一個公開介面，最後的操作要在執行時期才能決 定。
http://openhome.cc/Gossip/CppGossip/VirtualFunction.html

*/

class Foo1
{
public:
    virtual void show()
        {
        cout << "Foo1's show" << endl; 
        }
};

class Foo2 : public Foo1
{
public:
    virtual void show()
        {
        cout << "Foo2's show" << endl; 
        }
};

void showFooByPtr(Foo1 *foo)
{
    foo->show();
}

void showFooByRef(Foo1 &foo)
{
    foo.show();
}

int main()
{
    Foo1 f1;
    Foo2 f2;

    
    // 動態繫結 
       showFooByPtr(&f1); 
       showFooByPtr(&f2);
       cout << endl;
    
       // 動態繫結 
       showFooByRef(f1); 
       showFooByRef(f2);
       cout << endl; 
    
       // 靜態繫結 
       f1.show(); 
       f2.show(); 

    return 0;    
}
