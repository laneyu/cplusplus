#include <iostream>

/*
   构造顺序: 基类-> 子类
   析构顺序: 子类-> 基类
*/

using namespace std;

class Foo1
{
public:
    Foo1()
        {
        cout << "Foo1 constructor" <<endl;
        }
    ~Foo1()
        {
        cout << "Foo1 destructor" <<endl;
        }
};

class Foo2 : public Foo1
{
public:
    Foo2()
        {
        cout << "Foo2 constructor" <<endl;
        }
    ~Foo2()
        {
        cout << "Foo2 destructor" <<endl;
        }
};

int main()
{
    Foo2 f;

    cout << endl;

    return 0;
}

