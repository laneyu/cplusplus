#ifndef __TEST_H__
#define __TEST_H__

class Ball;

class Test
{
public:
    Test();
    Test(Ball *);

    Ball* ball();
    void ball(Ball *);
//protected:
//    int a;    //public能被直接访问, private 和protected则不能，编译报错
    
private:
    Ball *_ball;
};

#endif
