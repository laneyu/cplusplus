#include <iostream>
#include <memory>
#include "SafeArray.h"

/*
http://openhome.cc/Gossip/CppGossip/autoPtr.html
*/

using namespace std;

void test1()
{
    auto_ptr<int> iPtr;

    if (iPtr.get() == 0)
    {
        iPtr.reset(new int(1000));
    }
    cout << *iPtr << endl;
}

int main()
{
    auto_ptr<int>  iPtr (new int(100));
    auto_ptr<string> sPtr (new string("caterpillar"));

    cout << *iPtr << endl;
    if (sPtr->empty())
        cout << "�ִ����" << endl;
    else
        cout << "�ִ�: "<<*sPtr<<endl;

    test1();
}
