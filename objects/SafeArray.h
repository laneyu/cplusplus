#ifndef __SAFEARRAY_H__
#define __SAFEARRAY_H__

class SafeArray
{
public:
    SafeArray(int);
    ~SafeArray();

    int get(int);
    void set(int, int);

    int length;
    
private:
    int *_array;
    bool isSafe(int i);
};

#endif