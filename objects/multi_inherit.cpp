#include <iostream>

using namespace std;

class FooA
{
public:
    FooA()
        {
        cout << "FooA constructor"<<endl;
        }
    ~FooA()
        {
        cout << "FooA destructor" <<endl;
        }
};

class FooB
{
public:
    FooB()
        {
        cout << "FooB constructor"<<endl;
        }
    ~FooB()
        {
        cout << "FooB destructor" <<endl;
        }
};

class FooC : public FooA, public FooB
{
public:
    FooC()
            {
            cout << "FooC constructor"<<endl;
            }
     ~FooC()
            {
            cout << "FooC destructor" <<endl;
            }

};

int main()
{
    FooC c;

    cout << endl;

    return 0;
}

