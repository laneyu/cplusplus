#ifndef __ABSTRACT_CIRCLE_H__
#define __ABSTRACT_CIRCLE_H__

class AbstractCircle
{
public:
     void radius(double radius) 
        {
        _radius = radius;
        }
    double radius() 
        {
        return _radius;
        }

    virtual void render() = 0;

protected:
    double _radius;
};

#endif