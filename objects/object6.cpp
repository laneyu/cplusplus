#include <iostream>
#include "Cubic.h"

using namespace std;

int main()
{
    Cubic c1(0, 0, 0, 10, 20, 30);

    cout << "c1 volumn: "
           << c1.volumn()
           << endl;
//    cout << c1._length;   //protected 不能直接使用

    return 0;
}
