#ifndef __BALL_H__
#define __BALL_H__

/*
    当使用类别来定义一个物件(Object)时，考虑这个物件可能拥有的属性(property)
    与方法(method)，属性是物件的静态描述，而方法可以施加物件上的动态操作。
*/

using namespace std;

#include <string>

class Ball
{
public:
       Ball();
       Ball(double, const char *);
       Ball(double, string&);

       double radius();
       string& name();

       double volumn();
       void radius(double);
       void name(const char *);
       void name(string&);

private:
        double _radius; 
        string _name;
};


#endif