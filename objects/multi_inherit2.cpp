#include <iostream>

/*
    基类指针调用子类函数

    http://openhome.cc/Gossip/CppGossip/MultiInheritance2.html
*/
using namespace std;

class IRequest
{
public:
    virtual void execute() = 0;
};


class SomeObject
{
public:
    virtual void someFunction()
        {
            cout << "do something" <<endl;
        }

private:
    void otherFunction()
        {
            cout << "do other" <<endl;
        }
};

class Welcome : public SomeObject, public IRequest
{
public:
    void execute()
        {
            cout << "Welcome!" <<endl;
        }
};

class Hello : public SomeObject, public IRequest
{
public:
    void execute()
        {
            cout << "hello!" <<endl;
        }
};

void doRequest(IRequest *request)
{
    request->execute();
}

int main()
{
    Welcome welcome;
    Hello hello;

    doRequest(&welcome);
    doRequest(&hello);

    return 0;
}