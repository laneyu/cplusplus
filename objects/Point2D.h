#ifndef __POINT2D_H__
#define __POINT2D_H__

class Point2D
{
public:
    Point2D();
    Point2D(int, int);
    int x() {return _x;}
    int y() {return _y;}
    void x(int x) {_x = x;}
    void y(int y) {_y = y;}
    Point2D operator+(const Point2D&);  //重载+运算子
    Point2D operator-(const Point2D&);  //重载-运算子
    Point2D& operator++();  //重载++前置，如++p
    Point2D operator++(int);   //重载++后置，如p++
    Point2D& operator--();
    Point2D operator--(int);
//    friend Point2D operator+(const Point2D&, int);  //例如p+10
//    friend Point2D operator+(int, const Point2D&);  //例如10+p

private:
    int _x;
    int _y;
};

#endif