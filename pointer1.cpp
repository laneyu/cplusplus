#include <iostream>

using namespace std;

/*
    1.2 const int * 转换为int *直接编译出错 
*/

void foo(const int *);

int main()
{
    //1 1.1
    int var = 10;
    void *vptr = &var;

    int *iptr = reinterpret_cast<int *>(vptr);
    cout << *iptr<<endl;

    //1 1.2
    const int var12 = 10;
//    int *iptr12 = &var12;

    //1 1.3
    int var13 = 10;
    cout << "var13 = " << var13 <<endl;
    foo(&var13);
    cout << "var13 = " << var13 <<endl;
    return 0;
}

void foo(const int *p)
{
    int *v = const_cast<int *>(p);
    *v = 20;
}
