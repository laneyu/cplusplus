#include <iostream>

using namespace std;

int main()
{
    //1 1.1
    int var = 10;
    int &ref = var;

    cout << "var: " << var 
         << endl;
    cout << "ref: " << ref
         << endl;

    ref = 20;

    cout << "var: " << var 
         << endl;
    cout << "ref: " << ref
         << endl;

    //1 1.2
    const int &ref12 = 10;
    cout << "ref12 :" << ref12
           << endl;

    return 0;
}
