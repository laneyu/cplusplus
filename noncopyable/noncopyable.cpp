#include <iostream>

using namespace std;

class noncopyable
{
public:
    noncopyable()
        {
            cout << "constructor" <<endl;
        }
    ~noncopyable()
        {
            cout << "destructor" <<endl;
        }

    noncopyable(const noncopyable &a)
        {
            cout <<"copy constructor" <<endl;
        }

    noncopyable& operator=(const noncopyable &a)
        {
            cout <<"= override" <<endl;
        }
    
};


class ClassA :  noncopyable
{
};

void test_noncopyable()
{
    noncopyable a;          //构造函数
    noncopyable b(a);      //拷贝构造函数
    noncopyable d = a;    //拷贝构造函数
    noncopyable c;          //构造函数
    c = a;                      //赋值操作
        
    cout << endl;
}

void test_ClassA()
{
    ClassA a;
    ClassA b(a);
    ClassA d = a;
    ClassA c;

    c = a;
    cout << endl;
}



int main(int argc, char *argv[])
{
    test_noncopyable();
    cout << endl;
    test_ClassA();
}
