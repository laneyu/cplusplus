//	http://www.ibm.com/developerworks/cn/aix/library/1307_lisl_c11/

#include <iostream>
#include <utility>

bool is_r_value(int&&)
{
	return true;
}

bool is_r_value(const int&)
{
	return false;
}

void test(int&& i)
{
	std::cout <<is_r_value(i);
	std::cout <<is_r_value(std::move<int&>(i));
}

int main()
{
	test(10);
	
	int a;
	test(std::move<int &>(a));
}
