#include <iostream>

void process_value(int i)
{
	std::cout << "LValue processed: " << i << std::endl; 
}

template <typename T> void forward_value(T&& val)
{
	process_value(val);
}

//void process_value(const int& i)
//{
//}

int main()
{
	int a = 0;
	const int &b = 1;
	
	forward_value(a);
	forward_value(b);
}