#include <iostream>
#include <stdlib.h>

using namespace std;

void someFunctionMayCallExit()
{
    exit(1);
}

class GlobalObject
{
public:
    void doit()
    {
        cout << "GlobalObject::GlobalObject"<<endl;
        someFunctionMayCallExit();
    }

    ~GlobalObject()
    {
        cout << "GlobalObject::~GlobalObject"<<endl;
    }
};

GlobalObject g_obj;

int main()
{
    g_obj.doit();
    while(1);
}