#include <iostream>

#include <boost/shared_ptr.hpp>
#include <boost/weak_ptr.hpp>
#include <boost/scoped_ptr.hpp>

class X
{
public:
	X()
	{
		std::cout <<" X::constructor\n";
	}
	~X()
	{
		std::cout <<"~X::deconstructor\n";
	}
	void print()
	{
		std::cout <<"This address " <<this <<"\n";
	}
};


void unsafety1()
{
	boost::shared_ptr<X> p(new X);
	
	//some time later
	
	if (X* r = p.get())
	{
		p.reset();
		//use *r
		r->print();
	}
}

int main()
{
	unsafety1();
}

//result
// X::constructor
//~X::deconstructor
//This address 0x9a88008