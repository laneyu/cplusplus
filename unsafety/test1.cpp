#include <iostream>

#include <boost/shared_ptr.hpp>
#include <boost/weak_ptr.hpp>

class X
{
public:
	X()
	{
		std::cout <<" X::constructor\n";
	}
	~X()
	{
		std::cout <<"~X::deconstructor\n";
	}
};

int main()
{
	X x;
}

//result
// X::constructor
//~X::deconstructor