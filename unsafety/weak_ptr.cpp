#include <iostream>

#include <boost/shared_ptr.hpp>
#include <boost/weak_ptr.hpp>
#include <boost/scoped_ptr.hpp>

class X
{
public:
	X()
	{
		std::cout <<" X::constructor\n";
	}
	~X()
	{
		std::cout <<"~X::deconstructor\n";
	}
	void print()
	{
		std::cout <<"This address " <<this <<"\n";
	}
};


void unsafety1()
{
	boost::shared_ptr<X> p(new X);
	
	//some time later
	
	if (X* r = p.get())
	{
		p.reset();
		//use *r
		r->print();
	}
}

void safety1()
{
	boost::shared_ptr<X> p(new X);
	boost::weak_ptr<X> q(p);
	
	//some time later

	p.reset();
	if (boost::shared_ptr<X> r = q.lock())
	{
//		p.reset();
		//use *r
		r->print();
	}
}

int main()
{
	boost::scoped_ptr<X> p1(new X);
	boost::scoped_ptr<X> p2(new X);
	
	std::cout<<"swap before\n";
	p1->print();
	p2->print();
	
//	p1.swap(p2);
	swap(p1, p2);
	std::cout<<"swap after\n";
	p1->print();
	p2->print();
}

//result
// X::constructor
//~X::deconstructor