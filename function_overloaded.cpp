#include <iostream>

using namespace std;

/*
    overload 减少函数名称命名烦恼
*/

void showpara(int);
void showpara(float);
void showpara(int, int);

int main()
{
    showpara(10);
    showpara(static_cast<float>(10.0));     //10.0  默认为double类型
    showpara((float)(10.0));
 //   showpara(reinterpret_cast<float>(10.0));  //编译出错
    showpara(20, 30);

    return 0;
}

void showpara(int x)
{
    cout << "引数: " << x
           << endl;
}

void showpara(int x, int y)
{
    cout << "引数: " << x
           << "\t" << y
           << endl;
}

void showpara(float x)
{
    cout << "float 引数:" <<showpoint<< x
           << endl;
}