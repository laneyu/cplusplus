#include <stdio.h>
#include <time.h>

void print_struct_tm(struct tm *t)
{
	const int kSecondsPerHour = 60*60;
	
	printf("tm_sec	%d\n", t->tm_sec);
	printf("tm_min	%d\n", t->tm_min);
	printf("tm_hour	%d\n", t->tm_hour);
	printf("tm_mday	%d\n", t->tm_mday);
	printf("tm_mon	%d\n", t->tm_mon);
	printf("tm_wday	%d\n", t->tm_wday);
	printf("tm_yday	%d\n", t->tm_yday);
	printf("tm_isdst	%d\n", t->tm_isdst);
	printf("tm_gmtoff	%ld\n", t->tm_gmtoff/kSecondsPerHour);
	printf("tm_zone	%s\n", t->tm_zone);
}

int main()
{
	time_t now = time(NULL);
	struct tm t1 = *gmtime(&now);
	struct tm t2 = *localtime(&now);
	
	print_struct_tm(&t1);
	printf("\n");
	print_struct_tm(&t2);
}