#include <stdio.h>
#include <time.h>
#include <stdlib.h>	//getenv

const int kSecondPerHour = 60*60;

void get_timezone_name()
{
	printf("tzname[0]:%s\n", tzname[0]);
	printf("tzname[1]:%s\n", tzname[1]);
	printf("timezone:%ld(%ld)\n", timezone, timezone/kSecondPerHour);
	printf("daylight:%d\n", daylight);
}

void print_env(const char* env)
{
	char *p = getenv(env);
	if (p)
		printf("%s:%s\n", env, p);
	else
		printf("%s:%p\n", env, p);
}
void get_time_env()
{
	const char* kTZ = "TZ";
	const char* kTZDIR = "TZDIR";
	
	print_env(kTZ);
	print_env(kTZDIR);
}

void get_current_time()
{
	time_t now;
	struct tm utc;
	
	now = time(NULL);
	printf("%ld\n", now);
	printf("%s", ctime(&now));
	
	memcpy(&utc, localtime(&now), sizeof(utc));
	printf("%s\n", utc.tm_zone);
}

int main()
{
	get_timezone_name();		//current GMT-8
	get_current_time();
	get_time_env();
	
#if 0
	printf("\n\n");
	
	putenv("TZ=:Pacific/Auckland");
	putenv("TZDIR=/usr/share/zoneinfo");
	get_timezone_name();
	get_current_time();
	get_time_env();
	
	printf("\n\n");
	
	putenv("TZ=:Asia/Chongqing");
	putenv("TZDIR=/usr/share/zoneinfo");
	get_timezone_name();
	get_current_time();
	get_time_env();
#endif
	printf("\n\n");			//GMT
	if (putenv("TZ=GMT") == -1)
	{
		printf( "Unable to set TZ/n" );
		exit( 1 );
	}
//	putenv("TZDIR=/usr/share/zoneinfo");
	tzset();
	get_timezone_name();
	get_current_time();
	get_time_env();
	
		printf("\n\n");			//NZDT //GMT-12
	if (putenv("TZ=NZST12") == -1)
	{
		printf( "Unable to set TZ/n" );
		exit( 1 );
	}
//	putenv("TZDIR=/usr/share/zoneinfo");
	tzset();
	get_timezone_name();
	get_current_time();
	get_time_env();
	
		printf("\n\n");			
	if (putenv("TZ=AHST") == -1)
	{
		printf( "Unable to set TZ/n" );
		exit( 1 );
	}
//	putenv("TZDIR=/usr/share/zoneinfo");
	tzset();
	get_timezone_name();
	get_current_time();
	get_time_env();
}