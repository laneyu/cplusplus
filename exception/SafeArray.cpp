#include "SafeArray.h"
#include "ArrayIndexOutOfBoundsException.h"

#include <iostream>

using namespace std;

#if 0
SafeArray::SafeArray(int len)
{
    length = len;
    _array = new int[length];
    cout << "enter"<<endl;
}
#else
//1 使用成员初始化列表，多个用逗号分开
SafeArray::SafeArray(int len) : length(len)
{
    _array = new int[length];
    cout << "enter"<<endl;
}
#endif

bool SafeArray::isSafe(int i)
{
    if (i >= length || i < 0)
        return false;
    else
        return true;
}

int SafeArray::get(int i)
{
    if (isSafe(i))
        return _array[i];
    else
        throw ArrayIndexOutOfBoundsException(i);
}

void SafeArray::set(int i, int value)
{
    if (isSafe(i))
        _array[i] = value;
    else
        throw ArrayIndexOutOfBoundsException(i);
}

//1没有定义destructor则程序会自动建立一个没有内容的destructor
SafeArray::~SafeArray()
{
    delete [] _array;
    cout << "leave"<<endl;
}

int main()
{
    SafeArray safeArray(10);
    
       try {
           // 故意存取超過陣列長度
           for(int i = 0; i <= safeArray.length; i++) {
               safeArray.set(i, (i + 1) * 10);
           }
    
           for(int i = 0; i < safeArray.length; i++) {
               cout << safeArray.get(i) << " ";
           }
    
           cout << endl;
       }
       catch(ArrayIndexOutOfBoundsException e) {
           cout << endl
                << e.message()
                << endl;
       }
       catch(Exception e) {
           cout << endl
                << e.message()
                << endl;
       }
    
       return 0;

}
