//  http://www.cplusplus.com/reference/locale/isupper/

#include <iostream>
#include <string>
#include <locale>   //std::locale std::lower

int main()
{
    std::locale loc;
    std::string str= "Test String";
    for (std::string::size_type i = 0; i < str.length(); ++i)
    {
        if (std::isupper(str[i], loc))
        {
            str[i] = std::tolower(str[i], loc);
        }
    }

    std::cout<<str+"\n";
    
    return 0;
}