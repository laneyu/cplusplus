//  http://www.cplusplus.com/reference/locale/tolower/

#include <iostream>
#include <string>
#include <locale>   //std::locale std::lower

int main()
{
    std::locale loc;
    std::string str= "Test String";
    for (std::string::size_type i = 0; i < str.length(); ++i)
        std::cout <<std::tolower(str[i], loc);

    std::cout<<"\n";
    return 0;
}