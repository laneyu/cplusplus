//  http://www.cplusplus.com/reference/locale/toupper/

#include <iostream>
#include <string>
#include <locale>   //std::locale std::toupper

int main()
{
    std::locale loc;
    std::string str= "Test String";
    for (std::string::size_type i = 0; i < str.length(); ++i)
        std::cout <<std::toupper(str[i], loc);

    return 0;
}