#include <iostream>

class A
{
	int x;
public:
	operator int() {return x;}
};

class B : public A
{
	A y;
public:
	operator A() {return y;}
};

int main()
{
	B b_obj;
	
	int i = b_obj;
	int j = A(b_obj);
}
