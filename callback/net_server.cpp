#include <iostream>
#include <boost/bind.hpp>
#include <boost/function.hpp>
#include <boost/noncopyable.hpp>

#include <pthread.h>

class Connection
{
};
class NetServer : boost::noncopyable
{
public:
    typedef boost::function<void (Connection*)> ConnectionCallback;
    typedef boost::function<void (Connection*, const void *, int len)> MessageCallback;

    NetServer(uint16_t port);
    ~NetServer();
    void registerConnectionCallback(const ConnectionCallback&);
    void registerMessageCallback(const ConnectionCallback&);
    void sendMessage(Connection*, const void* buf, int len);

private:
    uint16_t port_;
};

class EchoServer
{
public:
    typedef boost::function<void (Connection*, const void *, int)> MessageCallback;

    EchoServer(MessageCallback sendMessageCb)
        :sendMessageCb_(sendMessageCb)
        {
        }
    void onMessage(Connection* conn, const void* buf, int size)
        {
        }
    void onConnection(Connection* conn)
        {
        }
private:
    MessageCallback sendMessageCb_;
};
