#include <iostream>
#include <boost/bind.hpp>
#include <boost/function.hpp>

class Penguin
{
public:
    void run()
        {
            std::cout <<"run" <<std::endl;
        }
    void swim()
        {
            std::cout <<"swim" <<std::endl;
        }
};

class Sparrow
{
public:
    void fly()
        {
            std::cout <<"fly" <<std::endl;
        }
    void run()
        {
            std::cout <<"run" <<std::endl;
        }
};

typedef boost::function<void ()> flyCallback;
typedef boost::function<void ()> runCallback;
typedef boost::function<void ()> swimCallback;

class Foo
{
public:
    Foo(runCallback runCb, swimCallback swimCb)
        :runCb_(runCb), swimCb_(swimCb)
        {
        }

    void run()
        {
            runCb_();
            swimCb_();
        }
    
private:
    runCallback runCb_;
    swimCallback swimCb_;
};

class Bar
{
public:
    Bar(flyCallback flyCb, runCallback runCb)
        :flyCb_(flyCb), runCb_(runCb)
        {
        }

    void run()
        {
            flyCb_();
            runCb_();
        }
    
private:
    flyCallback flyCb_;
    runCallback runCb_;
};

int main()
{
    Penguin p;
    Sparrow s;

    Foo foo(boost::bind(&Penguin::run, &p), boost::bind(&Penguin::swim, &p));
    Bar bar(boost::bind(&Sparrow::fly, &s), boost::bind(&Sparrow::run, &s));

    foo.run();

    bar.run();
}