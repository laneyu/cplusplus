#include <iostream>
#include <boost/bind.hpp>
#include <boost/function.hpp>
#include <pthread.h>


void *startThread(void *obj)
{
typedef boost::function<void()> ThreadCallback;
    ThreadCallback *func = (ThreadCallback *)obj;
    (*func)();
}

class Thread
{
public:
typedef boost::function<void()> ThreadCallback;

    Thread(ThreadCallback cb) 
        : cb_(cb)
        {
        }
    void start()
    {
        pthread_create(&thread_, NULL, startThread, &cb_);
    }

private:
    void run()
    {
        cb_();
    }

private:
    ThreadCallback cb_;
    pthread_t thread_;
};

class Foo
{
public:
    void runInThread();
    void runInAnotherThread(int);
};

void Foo::runInThread()
{
    while(1)
    {
        std::cout <<"runInThread" <<std::endl;
        sleep(1);
    }
}

void Foo::runInAnotherThread(int no)
{
    while(1)
    {
        std::cout <<"runInAnotherThread " <<no <<std::endl;
        sleep(1);
    }
}

int main()
{
    Foo foo;

    Thread thread1(boost::bind(&Foo::runInThread, foo));
    Thread thread2(boost::bind(&Foo::runInAnotherThread, foo, 50));

    thread1.start();
    thread2.start();

    while(1)
        {
             std::cout <<"main" <<std::endl;
            sleep(1);
        }
}