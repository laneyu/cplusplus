#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

/* 
    1.1 sort 默认从小到大排序，地址从小到大，否则出错
*/

int main()
{
     int iarr[] = {30, 12, 55, 31, 98, 11, 41, 80, 66, 21};

     vector<int> ivector(iarr, iarr+10);

     sort(ivector.begin(), ivector.end());
 //    sort(ivector.end(), ivector.begin());    //1.1

     for (vector<int>::iterator it = ivector.begin(); it != ivector.end(); it++)
    {
        cout << *it <<" ";
    }
     cout << endl;

     cout << "输入搜寻值:";
     int search = 0;
     cin >> search;

     vector<int>::iterator it = find(ivector.begin(), ivector.end(), search);

     if (it != ivector.end() )
     {
        cout << "找到搜寻值!"<<endl;
     }
     else
     {
        cout << "找不到搜寻值!"<<endl;
     }

     reverse(ivector.begin(), ivector.end());

     for (vector<int>::iterator it = ivector.begin(); it != ivector.end(); it++)
    {
        cout << *it <<" ";
    }
     cout << endl;

     return 0;
}
