//function pass class type means make a copy
/*  code result
A constructor
A copy constructor  
func1
*/

//往一个函数传递一个类值，则会make a copy, 调用拷贝构造函数
#include <iostream>

class A
{
public:
    A()
    {
        std::cout <<"A constructor" <<std::endl;
    }

    A(const A& a)
    {
        std::cout <<"A copy constructor" <<std::endl;
    }
};

void func1(A a)
{
    std::cout <<"func1" <<std::endl;
}

int main()
{
    A a;

    func1(a);
}
