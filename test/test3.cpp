//function pass class type means make a copy
//编译错误，注释掉main函数中func1(a);可以通过
//test3.cpp: In function ‘int main()’:
//test3.cpp:15:5: error: ‘A::A()’ is private
//test3.cpp:39:7: error: within this context
//test3.cpp:20:5: error: ‘A::A(const A&)’ is private
//test3.cpp:40:12: error: within this context
//test3.cpp:32:6: error:   initializing argument 1 of ‘void func1(A)’

//往一个函数传递一个类值，则会make a copy, 调用拷贝构造函数
//因为A中拷贝构造函数时私有的,这也是noncopyable的原理
#include <iostream>
#include <boost/noncopyable.hpp>

class A : boost::noncopyable
{
private:
    A()
    {
        std::cout <<"A constructor" <<std::endl;
    }

    A(const A& a)
    {
        std::cout <<"A copy constructor" <<std::endl;
    }

    A& operator=(const A& a)
    {
        std::cout <<"operator=" <<std::endl;
    }
};

//值传递方式虽说是copy，但是实际测试并未调用拷贝函数和赋值操作
void func1(A a)
{
    std::cout <<"func1" <<std::endl;
}

int main()
{
    A a;
    func1(a);
}
