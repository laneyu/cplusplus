#include <iostream>
#include <cstring>

using namespace std;

int main()
{
    char str[] = "hello";

    for (int i = 0; i < (sizeof(str)/sizeof(str[0])); i++)
    {
        if (str[i] == '\0')
            cout << " null";
        else
            cout << " " << str[i];
    }

    cout << endl;

    char str1[80] = {'\0'};
    char str2[] = "caterpillar";
    cout << "str1: " << str1 <<endl
           << "str2: " << str2 <<endl
           << endl;

    strcpy(str1, str2);
    cout << "str1: " << str1 <<endl
           << "str2: " << str2 <<endl
           << endl;

    strcat(str1, str2);
    cout << "str1: " << str1 <<endl
           << "str2: " << str2 <<endl
           << endl;

    cout << "str1长度:" << strlen(str1) <<endl
           << "str2 长度:" << strlen(str2) <<endl
           <<endl;

    cout << "str1与str2比较:" << strcmp(str1, str2) <<endl
            <<endl;

    return 0;
    return 0;
}
