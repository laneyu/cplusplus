#include <iostream>

#include "File.h"

static void wcharTochar(const wchar_t *wchar, char *chr, int length)   
{   
	WideCharToMultiByte( CP_ACP, 0, wchar, -1,  chr, length, NULL, NULL );   
} 
static void charTowchar(const char *chr, wchar_t *wchar, int size)   
{      
	MultiByteToWideChar( CP_ACP, 0, chr,strlen(chr)+1, wchar, size/sizeof(wchar[0]) );   
}  

//private function
bool FileHandle::FileOpen(const char *name, const int type)
{
	char tmp[1024];
	wchar_t wname[1024];

	if (type == 0)
	{
		_snprintf_s(tmp, sizeof(tmp), "%s%s", "////.//", name);
		charTowchar(tmp, wname, sizeof(wname));
		fileHandle_ = CreateFile(wname, GENERIC_READ|GENERIC_WRITE, 0, NULL, OPEN_EXISTING , 0, NULL);	//����
	}
	else
	{
		charTowchar(tmp, wname, sizeof(wname));
		fileHandle_ = CreateFile(wname, GENERIC_READ|GENERIC_WRITE, 0, NULL, OPEN_ALWAYS , 0, NULL);	//�ļ�
	}
	
	if (fileHandle_ == INVALID_HANDLE_VALUE)
		return false;
	else
		return true;
}

int FileHandle::FileWrite(const char *buf, const int len)
{
//	OVERLAPPED m_osWrite;
	DWORD dwBytesWrite = len;
	COMSTAT ComStat;
	DWORD dwErrorFlags;
	BOOL bWriteStat;

	if (fileHandle_ == INVALID_HANDLE_VALUE)
	{
		return -1;
	}

	ClearCommError(fileHandle_,&dwErrorFlags,&ComStat);
	bWriteStat=WriteFile(fileHandle_,buf,dwBytesWrite,&dwBytesWrite,NULL);

	return dwBytesWrite;
}

int FileHandle::FileRead(char *buf, int *len)
{
//	OVERLAPPED m_osRead;

	COMSTAT ComStat;
	DWORD dwErrorFlags;
	DWORD dwBytesRead = 1;
	BOOL bReadStat;

	if (fileHandle_ == INVALID_HANDLE_VALUE)
	{
		return -1;
	}

	ClearCommError(fileHandle_, &dwErrorFlags, &ComStat);
	dwBytesRead = min(dwBytesRead, (DWORD)ComStat.cbInQue);
	bReadStat = ReadFile(fileHandle_, buf, dwBytesRead, &dwBytesRead, NULL);

	return dwBytesRead;
}

FileHandle::FileHandle(const char *name, const int type)
{
	FileOpen(name, type);
}

FileHandle::~FileHandle()
{
    CloseHandle(fileHandle_);
}