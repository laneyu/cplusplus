#ifndef __FILE_H__
#define __FILE_H__

#include <windows.h>

class FileHandle
{
public:
    FileHandle(const char *name, const int type);
    ~FileHandle();
	int FileWrite(const char *buf, const int len);
	int FileRead(char *buf, int *len);

private:
	bool FileOpen(const char *name, const int type);
	
private:
    HANDLE fileHandle_;
};

#endif
