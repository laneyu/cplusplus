#include <iostream>

/*
http://wenku.baidu.com/view/507a6280d4d8d15abe234e7c

*/

using namespace std;

class BoolScope
{
public:
    BoolScope(bool& val, bool new_val) : m_val(val), m_old(val)
    {
        m_val = new_val;
    }

    ~BoolScope()
    {
        m_val = m_old;
    }

private:
    bool& m_val;
    bool m_old;
};

int test(bool &val)
{
    cout << "1111 " << val<<endl;    //0
    BoolScope bs_(val, true);
    cout << "2222 " << val<<endl;    // 1
}

int main()
{
    bool is_cacul = false;
    cout << "0000 " << is_cacul<<endl;   //0

    test(is_cacul);
    
    cout << "3333 " <<is_cacul<<endl;;    //0
    
}
