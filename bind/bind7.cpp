#include <assert.h>

#include <iostream>
#include <vector>
#include <algorithm>
#include <boost/bind.hpp>

struct F2
{
    int s;

    typedef void result_type;
    void operator()( int x ) 
        { 
        std::cout << x <<std::endl;
        s += x; 
        }
};


int main()
{
    F2 f2 = { 0 };
    int a[] = { 1, 2, 3 };
    
    std::for_each( a, a+3, bind( boost::ref(f2), _1 ) );    //ռλ����this
//    std::for_each(a, a+3, boost::bind(f2, _1));
    
    assert( f2.s == 6 );

}
