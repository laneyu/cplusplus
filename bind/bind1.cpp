/*
http://www.cppblog.com/mzty/archive/2007/09/05/31622.html

*/

#include <iostream>
#include <functional>
#include <string>
#include <boost/bind.hpp>

class some_class
{

public:
    void print_string(const std::string& s) const
        {
            std::cout << "some_class print_string" <<std::endl;
            std::cout <<s <<std::endl;
        }
    void print_classname()
        {
            std::cout <<"some_class" <<std::endl;
        }
};

void print_string(const std::string& s)
{
    std::cout <<"global print_string" <<std::endl;
    std::cout <<s <<std::endl;
}

void print_functionname()
{
    std::cout << "Print_functionname" <<std::endl;
}

int main()
{
    std::ptr_fun(&print_string)("hello1");  //把一般全局函数变为函数对象

    some_class sc0;
    std::mem_fun_ref(&some_class::print_classname)(sc0);    //把函数成员变为函数对象

    boost::bind(&print_string, _1)("Hello func!");
    boost::bind(&print_functionname);

    std::cout << std::endl;

    boost::bind(&some_class::print_classname, _1)(sc0);
    boost::bind(&some_class::print_string, _1, _2)(sc0, "hello world");
}
