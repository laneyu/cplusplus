/*
http://www.cppblog.com/mzty/archive/2007/09/05/31622.html

*/


//bind vitual class function
#include <functional>
#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include "boost/bind.hpp"
class base {
public:
    virtual void print() const 
    {    std::cout << "I am base.\n";  
    }  
    virtual ~base() {}
};
class derived : public base 
{
public: 
    void print()const 
    {    std::cout << "I am derived.\n";  }
};

int main()
{
    derived d;
    base b;
    boost::bind(&base::print,_1)(b);
    boost::bind(&base::print,_1)(d);

//    boost::bind(&derived::print, _1)(b);  //����
    boost::bind(&derived::print, _1)(d);
}

