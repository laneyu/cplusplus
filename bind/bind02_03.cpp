#include <iostream>

#include <boost/bind.hpp>

struct X
{
	int& get();
	int const& get() const;
};

int const& X::get() const
{
}

int main()
{
	boost::bind(static_cast<int const& (X::*) () const >(&X::get), _1);
	
	int const& (X::*get) () const = &X::get;
	std::cout <<get;
}

//error info
//bind02_03.cpp:13:25: error: call of overloaded ��bind(<unresolved overloaded function type>, boost::arg<1>&)�� is ambiguous