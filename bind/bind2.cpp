/*
http://www.cppblog.com/mzty/archive/2007/09/05/31622.html

*/


//bind instance or reference

#include <iostream>
#include <functional>
#include <string>
#include <boost/bind.hpp>

class tracer
{
public:
        tracer()
            {
            std::cout <<"tracer::tracer()\n";
            }
        
        tracer(const tracer& other)
            {
            std::cout << "tracer::tracer(const tracer& other)\n";
            }
        
        tracer& operator=(const tracer& other)
            {
            std::cout <<      "tracer& tracer::operator=(const tracer& other)\n";    
            return *this;
            }

        ~tracer()
            {
            std::cout << "tracer::~tracer()\n";
            }

        void print(const std::string& s)
            {
            std::cout << s << '\n'; 
            }
};

int main()
{
       tracer t;
       std::cout << std::endl;
       
       boost::bind(&tracer::print,t,_1)(std::string("I'm called on a copy of t\n"));
       std::cout << std::endl;
       
       tracer t1;
       std::cout << std::endl;
       
       boost::bind(&tracer::print,boost::ref(t1),_1)(  std::string("I'm called directly on t\n"));
       std::cout << std::endl;

}
