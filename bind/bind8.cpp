//Using bind with Boost.Function

#include <assert.h>

#include <iostream>
#include <vector>
#include <algorithm>
#include <boost/bind.hpp>
#include <boost/function.hpp>

class button
{
public:
    boost::function<void()> onClick;
};

class player
{
public:
    void play();
    void stop();
};

void player::play()
{
    std::cout <<"play"<<std::endl;
}

void player::stop()
{
    std::cout <<"stop"<<std::endl;
}

button playButton, stopButton;
player thePlayer;

void connect()
{
    playButton.onClick = boost::bind(&player::play, &thePlayer);    //绑定未调用
    stopButton.onClick = boost::bind(&player::stop, &thePlayer);
}

int main()
{
    connect();

    playButton.onClick();
    stopButton.onClick();

    boost::bind(&player::play, _1)(thePlayer);  //绑定，并且调用
    boost::bind(&player::play, &thePlayer)();    //绑定，并且调用
}
