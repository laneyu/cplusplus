#include <assert.h>

#include <iostream>
#include <vector>
#include <algorithm>
#include <boost/bind.hpp>
#include <boost/function.hpp>

int f(int a)
{
    std::cout <<a <<std::endl;
}

int main()
{
    boost::bind(f, _1);

//    boost::bind(f, _1)();   //error, ��������

    boost::bind(f, _1)("incompatible");
}
