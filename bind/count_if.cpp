// http://www.cplusplus.com/reference/algorithm/count_if/

// count_if example
#include <iostream>     // std::cout
#include <algorithm>    // std::count_if
#include <vector>       // std::vector

bool IsOdd(int i)
{
    return ((i%2) == 1);
}

int main()
{
    std::vector<int> myvector;

    for (int i = 1; i < 10; i++)
        myvector.push_back(i);

    int mycount = count_if(myvector.begin(), myvector.end(), IsOdd);

    std::cout<<"myvector contains "<<mycount <<" odd value"<<std::endl;

    return 0;
}
