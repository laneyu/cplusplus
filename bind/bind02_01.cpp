#include <iostream>

#include <boost/bind.hpp>

int f(int a)
{
}

int main()
{
	boost::bind(f, "incompatible")();
	boost::bind(f, _1)("incompatible");
}

//error info
///usr/include/boost/bind/bind.hpp:243:60: error: invalid conversion from ��const char*�� to ��int�� [-fpermissive]