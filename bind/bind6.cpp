#include <iostream>
#include <boost/bind.hpp>

class A
{
public:
    void func()
        {
            std::cout << "func print" <<std::endl;
        }
};


int main()
{
    
    A a;
    A& r = a;

    boost::bind(&A::func, a);
    boost::bind(&A::func, &a);
    boost::bind(&A::func, r);
    boost::bind(&A::func, _1);
}

