#include <iostream>
#include <algorithm>    //std::for_each
#include <vector>
#include <boost/bind.hpp>

class status
{
public:
    status(const std::string& name) : name_(name), ok_(true) {}
    void break_it()
        {
            ok_ = false;
        }
    bool is_broken() const
        {
            return ok_;
        }

    void report() const
        {
            std::cout << name_ << " is " <<
                (ok_ ? "working normally"  : "terribly broken") << '\n';
        }

private:
    std::string name_;
    bool ok_;
};

void boost_bind_method()
{
    std::vector<status> statuses;
    statuses.push_back(status("status 1"));
    statuses.push_back(status("status 2"));
    statuses.push_back(status("status 3"));
    statuses.push_back(status("status 4"));
    statuses[1].break_it();
    statuses[2].break_it();

    std::for_each(statuses.begin(), statuses.end(), boost::bind(&status::report, _1));
}

void boost_bind_class_member_func()
{
    status s0("status 0");

    boost::bind(&status::report, _1);   //�����
    boost::bind(&status::report, _1)(s0);
}

int main()
{
    boost_bind_method();

    std::cout << "--------------------------\n";
    boost_bind_class_member_func();
}
