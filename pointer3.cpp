#include <iostream>

using namespace std;

/*
    1.1  new/delete 以及初始化
    1.2  new/delete 数组
*/

int main()
{
    //1 1.1
    int *ptr = new int(100);

    cout << "空間位置:" << ptr 
         << endl; 
    cout << "空間儲存值：" << *ptr
         << endl; 

    *ptr = 200;
    
    cout << "空間位置:" << ptr 
         << endl; 
    cout << "空間儲存值：" << *ptr
         << endl; 

    delete ptr;

    //1 1.2
    int size = 0;

    cout << "請輸入陣列長度："; 
    cin >> size; 
    int *arr = new int[size];

    cout << "指定元素值:" <<endl;
    for (int i = 0; i < size; i++)
    {
        cout << "arr[" << i << "] = ";
        cin >> *(arr+i);
    }

    cout << "顯示元素值：" << endl; 
    for(int i = 0; i < size; i++) {
        cout << "arr[" << i << "] = " << *(arr+i)
             << endl; 
    } 

    delete [] arr;

    return 0;
}
