#include <map>

#include <boost/bind.hpp>
#include <boost/enable_shared_from_this.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/weak_ptr.hpp>

#include "Mutex.h"

#include <assert.h>
#include <stdio.h>

using std::string;

class Stock : boost::noncopyable
{
public:
    Stock(const string& name)
        :name_(name)
    {
        printf(" Stock[%p] %s\n", this, name_.c_str());
    }

    ~Stock()
    {
        printf("~Stock[%p] %s\n", this, name_.c_str());
    }

private:
    string name_;
};

namespace version1
{

class StockFacotry : boost::noncopyable
{
public:
    boost::shared_ptr<Stock> get(const string& key)
    {
        muduo::MutexLockGuard lock(mutex_);
        boost::shared_ptr<Stock>& pStock = stocks_[key];
        if (!pStock)
        {
            pStock.reset(new Stock(key));
        }
        return pStock;
    }

private:
    mutable muduo::MutexLock mutex_;
    std::map<string, boost::shared_ptr<Stock> > stocks_;
};

}

namespace version2
{

class StockFacotry : boost::noncopyable
{
public:
    boost::shared_ptr<Stock> get(const string& key)
    {
        boost::shared_ptr<Stock> pStock;
        muduo::MutexLockGuard lock(mutex_);
        boost::weak_ptr<Stock>& wkStock = stocks_[key];
        pStock = wkStock.lock();
        if (!pStock)
        {
            pStock.reset(new Stock(key));
            wkStock = pStock;
        }
        return pStock;
    }

private:
    mutable muduo::MutexLock mutex_;
    std::map<string, boost::weak_ptr<Stock> > stocks_;
};

}


int main()
{
    version1::StockFacotry sf1;
    version2::StockFacotry sf2;

    {
        boost::shared_ptr<Stock> s1 = sf1.get("stock1");
    }
    
    {
        boost::shared_ptr<Stock> s2 = sf2.get("stock2");
    }
}
