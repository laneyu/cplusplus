//  http://www.boost.org/doc/libs/1_55_0/libs/smart_ptr/example/shared_ptr_example.cpp

#include <vector>
#include <algorithm>
#include <set>
#include <iostream>
#include <boost/shared_ptr.hpp>

struct Foo
{
    Foo(int x) : x_(x) {}
    ~Foo() {std::cout <<"Destructing a Foo with x=" <<x_ <<"\n";}
    int x_;
};

typedef boost::shared_ptr<Foo> FooPtr;

struct FooPtrOps
{
    bool operator() (const FooPtr &a, const FooPtr &b)
        {
            return a->x_ > b->x_;
        }
    void operator() (const FooPtr &a)
        {
            std::cout <<a->x_ <<"\n";
        }
};

int main()
{
    std::vector<FooPtr> foo_vector;
    std::set<FooPtr, FooPtrOps> foo_set;

    FooPtr foo_ptr( new Foo(2));        //foo_ptr.use_count() == 1
    foo_vector.push_back(foo_ptr);      //+1
    foo_set.insert(foo_ptr);                //+1

    foo_ptr.reset(new Foo(1) );         //foo_ptr.use_cout() == 1
    foo_vector.push_back(foo_ptr);
    foo_set.insert(foo_ptr);

    foo_ptr.reset(new Foo(3) );
    foo_vector.push_back(foo_ptr);
    foo_set.insert(foo_ptr);

    foo_ptr.reset(new Foo(2) );
    foo_vector.push_back(foo_ptr);
    foo_set.insert(foo_ptr);

    std::cout <<"foo_vector:\n";
    std::for_each(foo_vector.begin(), foo_vector.end(), FooPtrOps());

    std::cout <<"foo_set:\n";
    std::for_each(foo_set.begin(), foo_set.end(), FooPtrOps());
    std::cout <<"\n";
//  Expected output:
//
//   foo_vector:
//   2
//   1
//   3
//   2
//   
//   foo_set:   //use_count只会为1，因为不是用的multiset
//   3
//   2
//   1
//
//   Destructing a Foo with x=2
//   Destructing a Foo with x=1
//   Destructing a Foo with x=3
//   Destructing a Foo with x=2
   
}