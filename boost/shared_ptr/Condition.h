#ifndef __CONDITION_H__
#define __CONDITION_H__

#include <pthread.h>
#include <assert.h>

#include <sys/types.h>
#include <unistd.h>

#include <boost/noncopyable.hpp>

#include "Mutex.h"

namespace muduo
{
class Condition : boost::noncopyable
{
public:
    explicit Condition(MutexLock& mutex) 
        :mutex_(mutex)
    {
        pthread_cond_init(&cond_, NULL);
    }
    ~Condition()
    {
        pthread_cond_destroy(&cond_);
    }

    void wait()
    {
        pthread_cond_wait(&cond_, mutex_.getPthreadMutex());
    }

    void notify()
    {
        pthread_cond_signal(&cond_);
    }

    void notifyAll()
    {
        pthread_cond_broadcast(&cond_);
    }
private:
    MutexLock& mutex_;
    pthread_cond_t cond_;
};
}

#endif
