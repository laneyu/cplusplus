#include "shared_ptr_example2.hpp"

#include <iostream>

int main()
{
    example a;
    a.do_something();
    std::cout<<"\n";
    example b(a);
    b.do_something();
    std::cout<<"\n";
    example c;
    c = a;  //deconstruct c
    c.do_something();
    std::cout<<"\n";
   return 0;
}
//finnish deconstruct a;

//
//use_count() is 1
//
//use_count() is 2
//
//destroying implementation
//use_count() is 3
//
//destroying implementation

