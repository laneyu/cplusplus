#include "shared_ptr_example2.hpp"
#include <iostream>

class example::implementation
{
public:
    ~implementation(){std::cout<<"destroying implementation\n";}
};

example::example()
    :imp_(new implementation)
{
}

void example::do_something()
{
    std::cout <<"use_count() is " <<imp_.use_count() <<"\n";
}