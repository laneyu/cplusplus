#ifndef __MUTEX_H__
#define __MUTEX_H__

#include <pthread.h>
#include <assert.h>

#include <sys/syscall.h>
#include <sys/types.h>
#include <unistd.h>

#include <boost/noncopyable.hpp>

#include <stdio.h>


namespace muduo
{
class MutexLock : boost::noncopyable
{
public:
    MutexLock()
        : holder_(0)
    {
        pthread_mutex_init(&mutex_, NULL);
    }
    ~MutexLock()
    {
        assert(holder_ == 0);
        pthread_mutex_destroy(&mutex_);
    }

    void lock()
    {
        pthread_mutex_lock(&mutex_);
        holder_ = getpid();
    }

    int isLockedByThisThread()
    {
        return (holder_ == getpid()) ? 1: 0;
    }

    pthread_mutex_t *getPthreadMutex()
    {
        return &mutex_;
    }

    void unlock()
    {
        holder_ = 0;
        pthread_mutex_unlock(&mutex_);
    }
private:
    pthread_mutex_t mutex_;
    pid_t holder_;
};

class MutexLockGuard : boost::noncopyable
{
public:
    explicit MutexLockGuard(MutexLock &mutex) : mutex_(mutex)
    {
        mutex_.lock();
    }

    ~MutexLockGuard()
    {
        mutex_.unlock();
    }

private:
    MutexLock& mutex_;
};

}

// Prevent misuse like:
// MutexLockGuard(mutex_);
// A tempory object doesn't hold the lock for long!
#define MutexLockGuard(x) static_assert(false, "missing mutex guard var name")

#endif
