#include <boost/shared_ptr.hpp>
#include <stdio.h>

#include "../../Mutex.h"

class Foo
{
public:
    Foo()
    {
        printf("Foo constructor\n");
    }
    ~Foo()
    {
        printf("Foo deconstrutor\n");
    }
};

muduo::MutexLock mutex;
boost::shared_ptr<Foo> globalPtr;

void test1()
{
    boost::shared_ptr<Foo> localPtr(new Foo);
}

void test2()
{
    boost::shared_ptr<Foo> localPtr(new Foo);
    globalPtr = localPtr;
}

void test3()
{
    boost::shared_ptr<Foo> localPtr1(new Foo);
    globalPtr = localPtr1;
    boost::shared_ptr<Foo> localPtr2(new Foo);
    printf("1\n");
    globalPtr = localPtr2;
    printf("1\n");
}


int main(int argc, char *argv[])
{
    test1();
    printf("--->\n");
    test2();
    printf("--->\n");
    test3();
    printf("--->\n");
}
