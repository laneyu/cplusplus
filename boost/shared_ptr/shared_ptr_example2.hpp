//Handle/Body Idiom (avoiding exposing the body(implementation) in the header file)
// http://www.boost.org/doc/libs/1_55_0/libs/smart_ptr/example/shared_ptr_example2.hpp

#include <boost/shared_ptr.hpp>

class example
{
public:
    example();
    void do_something();

private:
    class implementation;
    boost::shared_ptr<implementation> imp_; //hide implementation
};