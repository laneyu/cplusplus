//  http://man7.org/linux/man-pages/man2/syscall.2.html

#include <unistd.h>
#include <sys/syscall.h>
#include <sys/types.h>
#include <linux/unistd.h>
#include <stdio.h>

pid_t gettid()
{
    return static_cast<pid_t>(::syscall(SYS_gettid));
}

int main()
{
    printf("tid %d\n", gettid());
    while(1)
    {
    }
}
