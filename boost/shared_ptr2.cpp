#include <memory>
#include <iostream>

struct deleter
{
void operator()(int *p)
{
    delete p;
    std::cout << "-----------------\n";
}
};

int main()
{
    std::shared_ptr<int> sp0;

    std::cout << "(bool)sp0 == " << std::boolalpha 
        << (bool)sp0 << std::endl; 

    std::shared_ptr<int> sp1(new int(5)); 
    std::cout << "*sp1 == " << *sp1 << std::endl; 

    std::shared_ptr<int> sp2(new int(10), deleter()); 
    std::cout << "*sp2 == " << *sp2 << std::endl; 
}
