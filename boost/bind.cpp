#include <boost/bind.hpp>
#include <iostream>

using namespace std;
using namespace boost;

int f(int a, int b)
{
    cout << "a = " << a <<endl;
    cout << "b = " << b <<endl;
    return a+b;
}

struct O
{
    int operator()(int a, int b) {return a-b;}
    bool operator()(long a, long b) {return a==b;}
};

int test()
{
    O o;
    int x = 104;

    cout << bind<int>(o, _1, _1)(x) <<endl;
}

int main()
{
    bind(f, 1, 2)();    //f(1, 2)
    cout << endl;
    bind(f, _1, _2)(1, 2);  //f(1,2)
    cout << endl;
    bind(f, _2, _2)(1, 2);  //f(2, 2)
    cout << endl;
    bind(f, _1, 3)(1);        //f(1, 3)
    cout << endl;
    test();
}
