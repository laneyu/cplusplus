#include <boost/shared_ptr.hpp>
#include <cassert>
#include <iostream>

/*
删除共享对象
共有3个shared_ptr: a, b和temp，它们都指向同一个int实例。
*/

class A
{
    boost::shared_ptr<int> no_;

public:
    A(boost::shared_ptr<int> no) : no_(no){}
    void value(int i)
    {
        *no_ = i;
    }
};

class B
{
    boost::shared_ptr<int> no_;

public:
    B(boost::shared_ptr<int> no) : no_(no) {}
    int value() const 
    {
        return *no_;
    }
};

int main()
{
    boost::shared_ptr<int> temp(new int(14));
    std::cout << *temp<<"\r\n";

    A a(temp);
    B b(temp);

    a.value(28);

    std::cout << *temp<<"\r\n";
    assert(b.value() == 28);
}
