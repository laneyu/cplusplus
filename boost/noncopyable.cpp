#include <iostream>
#include <boost/noncopyable.hpp>

class NonCopyable : boost::noncopyable
{

};

class Copyable
{
public:
    Copyable()
    {
        std::cout << "Copyable -->" << "constructor"<<std::endl;
    }
    ~Copyable()
    {
        std::cout << "Copyable -->" << "destructor"<<std::endl;
    }
};

void test_copyable()
{
    Copyable a;
    Copyable b;
    b = a;
}

void test_noncopyable()
{
    NonCopyable a, b;
 //   NonCopyable c(a);   //error
 //   b = a;    //error
}

Copyable a;

int main()
{
 //  test_noncopyable();
//   test_copyable();
}
