#include <iostream>

using namespace std;

class A
{
public:
    A(int no) : no_(no) 
    {
        cout << "111111" << endl;
    }
    ~A()
    {
        cout << "222222" <<endl;
    }

private:
    int no_;
};

class B
{
public:
    B(A& a) : a_(a)
    {
        cout << "B 11"<<endl;
    }
    ~B()
        {
            cout << "B 22" <<endl;
        }

private:
    A& a_;
};

class C
{
public:
    C(A a) : a_(a)
    {
        cout << "C 11"<<endl;
    }
    ~C()
        {
            cout << "C 22" <<endl;
        }

private:
    A a_;
};


int main()
{
    
//    A(1);       //产生一个临时变量，马上又删除

    A b(1);

//    B c(b);

    C d(b);

    while (1);
}
