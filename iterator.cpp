#include <iostream>
#include <algorithm>
#include <vector>

int main()
{
    std::vector<int> myvector;

    for (int i = 0; i < 10; ++i)
        myvector.push_back(i);

    for (std::vector<int>::iterator it = myvector.begin(); it !=myvector.end(); ++it)
        std::cout << *it <<std::endl;
        
}
