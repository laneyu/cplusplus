#include <iostream>

using namespace std;

/*
    1.1 预设引数， 如果没有赋值，则采用默认值
    1.2 预设引数一旦出现在参数列表，那么其右边参数也必须设置
    double volumn(double h = 1.0, double w = 1.0, double l);    //错误
    double volumn(double l, double w = 1.0, double h = 1.0);    //正确
    1.3 预设引数的使用是已引数的顺序由左至右来进行的
*/

double area(double, double = 3.14);
bool setScreen(int  = 80, int  = 24, char =' ');

int main()
{
    //1 1.1
    double r, pi;

    cout << "输入半径 与PI: ";
    cin >> r >> pi;

    cout << "面积(自定义PI): " <<area(r, pi)
           << endl;

    cout << "面积(default): " << area(r)
           << endl;

    //1 1.2

    setScreen();
    setScreen(12);  //设置第一个参数
 //   setScreen(,12); //此方法错误
    
    return 0;
}

double area(double r, double pi)
{
    return r*r*pi;
}

bool setScreen(int width, int height, char bkcolor)
{
    cout << width << " " << height << " " << bkcolor<<endl;

    return true;
}

