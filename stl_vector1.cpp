#include <iostream>
#include <vector>

/*
    1.1 vector 会自动增长，每次增长为16
    1.2 当采用下标访问未初始化的vector时会出错，因为其capacity和size都为0
*/

using namespace std;

int main()
{
    vector<int> ivector;

    cout << "capacity 11: " << ivector.capacity()<<endl
            << "size 11: " << ivector.size()<<endl;
 //   cout << ivector[0]<<endl;   //导致错误验证1.2

    for (int i = 0; i < 17; i++)     //验证1.1, 可修改为16看看结果
    {
        ivector.push_back(i);
    }

    for (vector<int>::iterator it = ivector.begin(); it != ivector.end(); it++)
    {
        cout << *it << " ";
    }
    cout << endl;
    cout << "capacity: " << ivector.capacity()<<endl
            << "size: " << ivector.size()<<endl;

    return 0;
}
