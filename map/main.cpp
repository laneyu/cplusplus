#include <iostream>
#include <string>
#include <algorithm>
#include <map>
#include <set>

using namespace std;

int main()
{
    std::multimap<int, std::string> mymap;
    std::multimap<int, std::string>::iterator mymapIter;

    mymap.insert(std::pair<int, std::string>(0, "1024"));
    mymap.insert(std::pair<int, std::string>(0, "2048"));
    mymap.insert(std::pair<int, std::string>(5, "2048"));
    std::cout <<mymap.size() <<"\n";
    for (mymapIter = mymap.begin(); mymapIter != mymap.end(); ++mymapIter)
    {
        std::cout <<mymapIter->first <<"  " <<mymapIter->second <<"\n";
    }

    mymapIter = mymap.begin();
    mymap.erase(mymapIter);

    for (mymapIter = mymap.begin(); mymapIter != mymap.end(); ++mymapIter)
    {
        std::cout <<mymapIter->first <<"  " <<mymapIter->second <<"\n";
    }
    return 0;
}

