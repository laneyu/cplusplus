#include <iostream>

#include <stddef.h>

class Base
{
public:     //Test 1
//private:  //Test 2
    virtual void f()
    {
        std::cout <<"Base::f\n";
    }

    virtual void g()
    {
        std::cout <<"Base::g\n";
    }

    void h()
    {
        std::cout <<"Base::g\n";
    }
};

Base b;
typedef void (*Func)(void);
Func pFun;

int main()
{
    std::cout <<"virtual table address:" <<(int *)&b <<"\n";
    std::cout <<"virtual table -- first func address:" <<(int *)*(int *)&b <<"\n";
    std::cout <<"virtual table -- second func address:" <<(int *)*(int *)&b+1 <<"\n";

    pFun = (Func)*((int *)*(int *)&b);
    pFun();

    pFun = (Func)*((int *)*(int *)&b+1);
    pFun();

    return 0;
}

