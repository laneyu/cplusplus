#include <iostream>

class AA
{
public:
    void test()
    {
        std::cout <<"Class AA" <<std::endl;
    }
};

class BB
{
public:
    void test()
    {
        std::cout <<"Class BB" <<std::endl;
    }
};

int main()
{
//    AA *A = (AA *)(new BB());
    std::cout <<&BB::test;
    AA *A = (AA *)(new int(1));
    A->test();
    return 0;
}


