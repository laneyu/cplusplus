#include <iostream>
#include <vector>

class BigInteger
{
public:
    BigInteger(unsigned short);

    BigInteger(unsigned int);

    BigInteger(unsigned long);

    BigInteger(unsigned long long);


    BigInteger(const BigInteger& n);
    
    //operator
    BigInteger& operator+(const BigInteger& n);
    BigInteger operator*(const BigInteger& n);
    BigInteger operator=(const BigInteger& n);
    void output();

private:
    std::vector<unsigned short> value_;
};

void BigInteger::output()
{
    for (int i = value_.size() - 1; i >= 0 ; --i)
    {
        std::cout<<value_[i];
    }

    std::cout<<std::endl;
}

//[Low] ...... [High]
BigInteger::BigInteger(unsigned short value)
{
    //value is 0
    do     
    {
        value_.push_back(value%10);
        value = value/10;
    }while(value);
}

//[Low] ...... [High]
BigInteger::BigInteger(unsigned int value)
{
    //value is 0
    do     
    {
        value_.push_back(value%10);
        value = value/10;
    }while(value);
}

//[Low] ...... [High]
BigInteger::BigInteger(unsigned long value)
{
    //value is 0
    do     
    {
        value_.push_back(value%10);
        value = value/10;
    }while(value);
}

//[Low] ...... [High]
BigInteger::BigInteger(unsigned long long value)
{
    //value is 0
    do     
    {
        value_.push_back(value%10);
        value = value/10;
    }while(value);
}

//constructor
BigInteger::BigInteger(const BigInteger& n)
{
    for (int i = 0; i < n.value_.size(); i++)
    {
        value_[i] = n.value_[i];
    }
}

//operator+
BigInteger& BigInteger::operator+(const BigInteger& n)
{
    int min_digits = value_.size() < n.value_.size() ? value_.size() : n.value_.size();

    for (int i =  0; i < min_digits + 1; ++i)
    {
        if ((value_[i] + n.value_[i]) >= 10)
        {
            value_[i] = (value_[i] + n.value_[i])%10;
            value_[i+1] = value_[i+1] + (value_[i] + n.value_[i])/10;
        }
        else
        {
            value_[i] = value_[i] + n.value_[i];
        }
    }

    return *this;
}

//operator*
BigInteger BigInteger::operator*(const BigInteger& n)
{
    BigInteger result((unsigned int)0);
    
    for (int i = 0; i < value_.size(); ++i)
    {
        unsigned int sum = 0;
        unsigned int digit = 1;
        
        for (int j = 0; j < n.value_.size(); ++j)
        {
            
        
            if ((value_[i] * n.value_[j]) >= 10)
            {
            //    value_[i] = (value_[i] * n.value_[j])%10;
            //    value_[i+1] = value_[i+1] + (value_[i] * n.value_[j])/10;
            }
            else
            {  
                sum = sum + value_[i] * n.value_[j]*digit;
            }
            digit = digit*10;
        }

        BigInteger tmp(sum);

        tmp.output();
        result = result + tmp;
    }

    
    return result;
}

//operator=
BigInteger BigInteger::operator=(const BigInteger& n)
{
    BigInteger result((unsigned int)0);

    std::cout << "\n------------------------------------\n";
    std::cout << "size : " <<value_.size() <<"\n";
    for (int i = 0; i < n.value_.size(); i++)
    {
        result.value_.push_back(n.value_[i]);
    }

    result.output();

    std::cout << "\n------------------------------------\n";
    return result;
}

void test_positive ()
{
        unsigned short sa = 0xffff;
        std::cout <<sa<<std::endl;
    
        unsigned int a1 = 0xffffffff;
        std::cout <<a1<<std::endl;
    
        unsigned long a2 = 0xffffffff;
        std::cout <<a2<<std::endl;
    
        unsigned long long a3 = 0xffffffffffffffff;
        std::cout <<a3<<std::endl;
    
        BigInteger ba1(sa);
        ba1.output();
    
        BigInteger ba2(a1);
        ba2.output();
    
        BigInteger ba3(a2);
        ba3.output();
    
        BigInteger ba4(a3);
        ba4.output();
}

void test_operator()
{
    unsigned short a1 = 1;
    unsigned int a2 = 56780;

    BigInteger ba1(a1);
    BigInteger ba2(a2);
    BigInteger ba3(a1);

    ba3 = ba2 + ba1;
    std::cout<<"ba3: ";
    
    ba3.output();
}
int main()
{
    test_positive();

    std::cout<<"\n";
    test_operator();
}


  