//operator+ as a friend function, with access to all members of the class
//operator+ 携带两个参数

#include <iostream>

class Element
{
public:
    Element()
        {
            value_ = 0;
        }
    Element(int value)
        :value_(value)
        {}
    int getValue() const
    {
        return value_;
    }

    friend Element operator+(const Element& a, const Element& b);    
private:
    int value_;
};

// Left operand is 'a'; right is b
// e1 + e2 = operator+(e1, e2)
Element operator+(const Element& a, const Element& b)
{
    return Element(a.value_ + b.value_);
}


int main()
{
    Element a(1);
    Element b(2);
    Element c;

    c = a+b;

    std::cout <<"c: " <<c.getValue() <<std::endl;
}
