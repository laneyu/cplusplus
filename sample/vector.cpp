#include <iostream>
#include <algorithm>
#include <vector>

class Vector
{
public:
    void push_back(int);

    void output();
private:
    std::vector<int> vector_;
};


void Vector::push_back(int t)
{
    vector_.push_back(t);
}

void Vector::output()
{
    for (int i = 0; i < vector_.size(); i++)
    {
        std::cout <<vector_[i] <<std::endl;
    }
}

int main()
{
    Vector myvector;
    
    for (int i = 0; i < 10; i++)
        myvector.push_back(i);

    myvector.output();
}
