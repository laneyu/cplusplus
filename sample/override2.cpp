//operator+ as a member function, with access to all members of the class
//operator+ 一个或者零个参数

#include <iostream>

class Element
{
public:
    Element()
        {
            value_ = 0;
        }
    Element(int value)
        :value_(value)
        {}
    int getValue() const
    {
        return value_;
    }

    Element operator+(const Element& b);
    
private:
    int value_;
};

// Left operand is this; right is b
// e1 + e2 = e1.operator+(e2)
Element Element::operator+(const Element& b)
{
    return Element(value_ + b.value_);
}


int main()
{
    Element a(1);
    Element b(2);
    Element c;

    c = a+b;

    std::cout <<"c: " <<c.getValue() <<std::endl;
}
