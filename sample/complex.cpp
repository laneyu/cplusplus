// override <<
//http://stackoverflow.com/questions/8978202/operator-overloading
//http://stackoverflow.com/questions/15441633/number-of-arguments-in-operator-overload-in-c

#include <iostream>

class Complex
{
public:
    //构造函数
    Complex()
        :real_(0), imag_(0)
    {
    }
    
    Complex(double real, double imag)
        :real_(real), imag_(imag)
    {
    }
    //拷贝构造函数
    Complex(Complex &complex)
    {
        real_ = complex.real_;
        imag_ = complex.imag_;
    }

    std::ostream& output(std::ostream& s) const;

    //member function
    Complex operator+(Complex& c2);    //+
    Complex operator-(Complex& c2);     //-
    Complex operator*(Complex& c2);     //*
    Complex operator/(Complex& c2);     ///
    Complex& operator++();                  //prefix ++
    Complex operator++(int);                //postfix ++
    Complex& operator--();                  //prefix --
    Complex operator--(int);                //postfix --
    
private:
    double real_;
    double imag_;
};

std::ostream& Complex::output(std::ostream& s) const
{
    std::cout <<real_ <<" + " <<imag_ <<"i";
}

std::ostream& operator<<(std::ostream& cout, Complex& complex)
{
    return complex.output( cout );
}

//override +
Complex Complex::operator+(Complex& c2)
{
    Complex tmp;

    tmp.real_ = real_ + c2.real_;
    tmp.imag_ = imag_ + c2.imag_;

    return tmp;
}

//override -
Complex Complex::operator-(Complex& c2)
{
    Complex tmp;

    tmp.real_ = real_ - c2.real_;
    tmp.imag_ = imag_ - c2.imag_;

    return tmp;
}

//override *
Complex Complex::operator*(Complex& c2)
{
    Complex tmp;

    tmp.real_ = real_*c2.real_ - imag_*c2.imag_;
    tmp.imag_ = imag_*c2.real_ + real_*c2.imag_;

    return tmp;
}

//override /
Complex Complex::operator/(Complex& c2)
{
    Complex tmp;

    tmp.real_ = (real_*c2.real_ + imag_*c2.imag_)/(c2.real_*c2.real_+c2.imag_*c2.imag_);
    tmp.imag_ = (imag_*c2.real_ - real_*c2.imag_)/(c2.real_*c2.real_+c2.imag_*c2.imag_);

    return tmp;
}

// prefix ++
//Complex Complex::operator++()     //此种方式会多次数据copy
Complex& Complex::operator++()  
{
    real_ = real_ + 1;
    imag_ = imag_ + 1;

    return *this;
}

//postfix++
Complex Complex::operator++(int)
{
    Complex tmp(*this);

    ++(*this);

    return tmp;
}

// prefix --
Complex& Complex::operator--()  
{
    real_ = real_ - 1;
    imag_ = imag_ - 1;

    return *this;
}

//postfix--
Complex Complex::operator--(int)
{
    Complex tmp(*this);

    --(*this);

    return tmp;
}



void test1()
{
    Complex a(1, 1);
    Complex b(2, 2);
    Complex c;
    Complex d, e, f;


    std::cout <<"test1" <<std::endl;

    c = a+b;
    
    std::cout << c <<std::endl;

    d = b - a;
    std::cout << d <<std::endl;

    e = a*b;

    std::cout << e <<std::endl;

    f = a/b;
    std::cout << f <<std::endl;
}

void test2()
{
    Complex a(1, 1);
    Complex b(2, 2);
    Complex c;
    Complex d, e, f;

    std::cout <<"test2"<<std::endl;

    c = ++a;

    std::cout <<"c :" <<c <<std::endl;
    std::cout <<"a :" <<a <<std::endl;

    d = a++;

    std::cout <<"d :" <<d <<std::endl;
    std::cout <<"a :" <<a <<std::endl;

    c = --a;

    std::cout <<"c :" <<c <<std::endl;
    std::cout <<"a :" <<a <<std::endl;

    d = a--;

    std::cout <<"d :" <<d <<std::endl;
    std::cout <<"a :" <<a <<std::endl;
}

int main()
{
    test1();
    test2();
}
