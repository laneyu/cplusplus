#include <iostream>
#include <string.h>

class String
{
public:
    String()
    {
        s_ = new char[1];
        s_ = '\0';
    }

    String(const char *s)
    {
        s_ = new char[strlen(s) + 1];
        strcpy(s_, s);
    }

    String(const String& s0)
    {
        s_ = new char[strlen(s0.s_) + 1];
        strcpy(s_, s0.s_);
    }

    ~String()
    {
        delete [] s_;
    }

    void ToString()
    {
        std::cout <<"ToString ---> " <<s_ <<std::endl;
    }

    String& StrCpy(const char *s)
    {
        delete [] s_;
        s_ = new char[strlen(s) + 1];
        strcpy(s_, s);

        return *this;
    }

    String& StrCat(String& s0)
    {
        char *p;

        p = new char[strlen(s_) + 1];
        strcpy(p, s_);
        delete [] s_;

        s_= new char[strlen(s0.s_) + strlen(p) + 1];
        strcpy(s_, p);
        strcat(s_, s0.s_);

        return *this;
    }

    void StrCmp(String& s0, String& s1)
    {
        if (strcmp(s0.s_, s1.s_) > 0)
            std::cout <<s0.s_ <<">" <<s1.s_ <<std::endl;
        else if (strcmp(s0.s_, s1.s_) < 0)
            std::cout <<s0.s_ <<"<" <<s1.s_ <<std::endl;
        else
            std::cout <<s0.s_ <<"=" <<s1.s_ <<std::endl;
    }

private:
    char *s_;
};

int main()
{
    String s1("12345");
    String s2("12345");
    String s3;

    s3.StrCmp(s1, s2);

    s1.ToString();

    s1.StrCpy("abcde");

    s1.ToString();

    s1.StrCat(s2);
    s1.ToString();

    
}
