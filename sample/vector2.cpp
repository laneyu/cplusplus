#include <iostream>
#include <algorithm>
#include <vector>

template <typename T>
class Vector
{
public:
    void push_back(T);

    void output();
private:
    std::vector<T> vector_;
};

template <typename T>
void Vector<T>::push_back(T t)
{
    vector_.push_back(t);
}

template <typename T>
void Vector<T>::output()
{
    for (int i = 0; i < vector_.size(); i++)
    {
        std::cout <<vector_[i] <<std::endl;
    }
}

int main()
{
    Vector<int> myvector;

    for (int i = 0; i < 10; i++)
        myvector.push_back(i);

    myvector.output();

    Vector<double> myvector2;
    double tmp = 999.91;
    for (int i = 0; i < 10; i++)
    {
        tmp++;
        myvector2.push_back(tmp);
    }
    myvector2.output();
}
