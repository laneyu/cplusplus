#include <iostream>
#include <vector>

using namespace std;

int main()
{
    vector<int> ivector1(5, 1);
    vector<int> ivector2;

    ivector2 = ivector1;

    for (int i = 0; i < ivector2.size(); i++)
    {
        cout << ivector2[i] <<" ";
    }
    cout << endl;

    cout << &ivector1[0] <<"  "<< &ivector2[0]<<endl;

    ivector2[0] = 2;
     for (int i = 0; i < ivector2.size(); i++)
    {
        cout << ivector2[i] <<" ";
    }
    cout << endl;

    for (int i = 0; i < ivector1.size(); i++)
    {
        cout << ivector1[i] <<" ";
    }
    cout << endl;
}
