#include <iostream>

#include <string.h>

class Cents
{
public:
    int m_nCents;
public:
    Cents(int nCents=0)
    {
        m_nCents = nCents;
    }
};

void shallow_copy1()
{
	Cents a1(10);
	Cents a2(a1);
	
	std::cout <<"shallo_copy1\n";
	
	std::cout <<"a1 " <<a1.m_nCents <<"\n";
	std::cout <<"a2 " <<a2.m_nCents <<"\n";
	
	std::cout <<"\n";
}

class MyString
{
public:
    char *m_pchString;
    int m_nLength;
 
public:
    MyString(char *pchString="")
    {
        // Find the length of the string
        // Plus one character for a terminator
        m_nLength = strlen(pchString) + 1;
 
        // Allocate a buffer equal to this length
        m_pchString= new char[m_nLength];
 
        // Copy the parameter into our internal buffer
        strncpy(m_pchString, pchString, m_nLength);
 
        // Make sure the string is terminated
        m_pchString[m_nLength-1] = '\0';
    }
	
	MyString(const MyString& cSource)
	{
		// because m_nLength is not a pointer, we can shallow copy it
		m_nLength = cSource.m_nLength;
	 
		// m_pchString is a pointer, so we need to deep copy it if it is non-null
		if (cSource.m_pchString)
		{
			// allocate memory for our copy
			m_pchString = new char[m_nLength];
	 
			// Copy the string into our newly allocated memory
			strncpy(m_pchString, cSource.m_pchString, m_nLength);
		}
		else
			m_pchString = 0;
	}
	
	MyString& operator=(const MyString& cSource)
	{
		// check for self-assignment
		if (this == &cSource)
			return *this;
	 
		// first we need to deallocate any value that this string is holding!
		delete[] m_pchString;
	 
		// because m_nLength is not a pointer, we can shallow copy it
		m_nLength = cSource.m_nLength;
	 
		// now we need to deep copy m_pchString
		if (cSource.m_pchString)
		{
			// allocate memory for our copy
			m_pchString = new char[m_nLength];
	 
			// Copy the parameter the newly allocated memory
			strncpy(m_pchString, cSource.m_pchString, m_nLength);
		}
		else
			m_pchString = 0;
	 
		return *this;
	}
 
    ~MyString() // destructor
    {
        // We need to deallocate our buffer
        delete[] m_pchString;
 
        // Set m_pchString to null just in case
        m_pchString = 0;
    }
 
    char* GetString() { return m_pchString; }
    int GetLength() { return m_nLength; }
};

void shallow_copy2()
{
	MyString cHello("Hello, world!");
	
	std::cout <<"cHello m_pchString addr:" <<cHello.m_pchString <<"\n";
 
	{
		MyString cCopy = cHello; // use default copy constructor			//1
		std::cout <<"cHello m_pchString addr:" <<cHello.m_pchString <<"\n";
		std::cout <<"cCopy m_pchString addr:" <<cCopy.m_pchString <<"\n";
	} // cCopy goes out of scope here cCopy									//2
	
	std::cout <<"cHello m_pchString addr:" <<cHello.m_pchString <<"\n";
 
	std::cout << cHello.GetString() << std::endl; // this will crash
//result
//cHello m_pchString addr:Hello, world!
//cHello m_pchString addr:Hello, world!
//cCopy m_pchString addr:Hello, world!
//cHello m_pchString addr:

//reason
//1 shallow copy
//2 cCopy deconstruct & delete cCopy malloc memory
}

int main()
{
	shallow_copy1();
	
	shallow_copy2();
}