#include <iostream>

using namespace std;

struct StructA
{
};

class ClassA
{
public:
    ClassA(int a)
        : a_(a)
        {
            cout << "constructor a = " <<a_<<endl;
        }

    ClassA(const ClassA& a)
        {
            cout << "copy constructor" <<endl;
            a_ = a.a_;
        }

    ClassA& operator=(ClassA& a)    //重载赋值操作符"="
        {
            cout << "opetrator" <<endl;
            a_ = a.a_;

            return *this;
        }
    void show()
        {
            cout << a_ <<endl;
        }

private:
    int a_;
};

int main(int argc, char *argv[])
{
    ClassA a(2);        //构造函数
    ClassA c(c);        //拷贝构造函数

    ClassA b = a;      //拷贝构造函数

    c.show();
    c = a;
    c.show();
}
