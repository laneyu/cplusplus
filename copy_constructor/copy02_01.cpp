// shallow copy
#include <iostream>

class X
{
public:
	int i;
	int* pi;
	
public:
	X()
	{
		i = 0;
		pi = new int(5);
	}
	
	X(const X& copy)
	:i(copy.i),
	pi(copy.pi)
	{
	}
};

int main()
{
	X x;
	X y(x);
	
	std::cout <<"x info\n";
	std::cout <<"x.i addr: " <<x.i <<"\n";
	std::cout <<"&x.pi addr: " <<&x.pi <<"\n";
	std::cout <<"x.pi addr: " <<x.pi <<"\n";
	std::cout <<"*x.pi addr: " <<*x.pi <<"\n";
	std::cout <<"\n";
	
	std::cout <<"y info\n";
	std::cout <<"y.i addr: " <<y.i <<"\n";
	std::cout <<"&y.pi addr: " <<&y.pi <<"\n";
	std::cout <<"y.pi addr: " <<y.pi <<"\n";
	std::cout <<"*y.pi addr: " <<*y.pi <<"\n";
	std::cout <<"\n";
	
	*y.pi = 10;
	
	std::cout <<"x info\n";
	std::cout <<"x.i addr: " <<x.i <<"\n";
	std::cout <<"&x.pi addr: " <<&x.pi <<"\n";
	std::cout <<"x.pi addr: " <<x.pi <<"\n";
	std::cout <<"*x.pi addr: " <<*x.pi <<"\n";
	std::cout <<"\n";
	
	std::cout <<"y info\n";
	std::cout <<"y.i addr: " <<y.i <<"\n";
	std::cout <<"&y.pi addr: " <<&y.pi <<"\n";
	std::cout <<"y.pi addr: " <<y.pi <<"\n";
	std::cout <<"*y.pi addr: " <<*y.pi <<"\n";
	std::cout <<"\n";
}

//result
//x info
//x.i addr: 0
//&x.pi addr: 0xbfd880e4
//x.pi addr: 0x85b9008
//*x.pi addr: 5
//
//y info
//y.i addr: 0
//&y.pi addr: 0xbfd880ec
//y.pi addr: 0x85b9008
//*y.pi addr: 5
//
//x info
//x.i addr: 0
//&x.pi addr: 0xbfd880e4
//x.pi addr: 0x85b9008
//*x.pi addr: 10
//
//y info
//y.i addr: 0
//&y.pi addr: 0xbfd880ec
//y.pi addr: 0x85b9008
//*y.pi addr: 10