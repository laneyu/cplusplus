#include <iostream>

using namespace std;

// 1. 构造函数抛出异常，会导致析构函数不能执行
// 2. 因为析构函数不能被调用，可能导致系统资源未被释放或者内存泄露
// 3. 构造函数可以抛出异常，但抛出异常之前必须保证系统资源被释放，防止内存泄露
class MyTest_Base
{
public:
    MyTest_Base(int& status)
    {
        std::cout <<"MyTest_Base constructor running\n";
        //执行操作
        //由于资源不够，构造失败
        //把status设置为0，通知构建者
		
		//抛出异常
		throw std::exception();
        status = 0;
    }
	
	~MyTest_Base()
	{
		std::cout <<"MyTest_Base destructor running\n";
	}
};

int main()
{
    int status = 1;
	try
	{
    MyTest_Base a1(status);
	}
	catch (exception& e)
	{
		cout << "Standard exception: " << e.what() << endl;
	}
    if (status == 0)
        cout << "construtor failed\n" << endl;
    return 0;
}

