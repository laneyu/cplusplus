#include <iostream>
#include <string>

/*
    1.1  相同字符串不同实例地址不一样
    1.2  可以用char型数字赋值给string 类型
    1.3  1.2相反情况就不行，编译直接出错
    
*/

using namespace std;

int main()
{
    //1 1.1
    string str1("text1");   
    string str2("text1");
    string str3 = str2;

    cout <<"str1地址:" << &str1 <<endl
           <<"str2地址:" << &str2 <<endl
           <<"str3地址:" << &str3 <<endl
           <<endl;

    //1 1.2
    string name("caterpillar");
    char str[] = "caterpillarjustin";
    name = str;
    cout << name<<endl;

    //1 1.3
    char str13[] = "justin";
    string name13("caterpillar");
//    str13 = name13;   

    //1 1.4
    string name14("caterpillar");
    for (int i = 0; i < name14.size(); i++)
    {
        cout << name14[i] <<endl;
    }

    //1 1.5
    string str15_1;
    string str15_2("caterpillar");
    string str15_3(str15_2);

    //assign 指定字符串
    str15_1 = str15_1.assign(str15_2, 0, 5);
    cout << "str15_1: " << str15_1 <<endl;
    str15_1 = str15_1.assign("caterpillar", 5, 6);
    cout << "str15_1: " << str15_1 <<endl;

    str15_1 = "";

    //append 字串串接
    str15_1 = str15_1.append(str15_2, 0, 5);
    str15_1 = str15_1.append(str15_3, 5, 6);
    cout << "str15_1: " << str15_1 <<endl;

    //find 搜寻字串位置
   cout << "寻找str15_1中的第一个pill: "
          << str15_1.find("pill", 0) <<endl;

    //insert 插入字串
    
    cout << "str15_1长度: " <<str15_1.length() <<endl;
    cout << "在str15_1中插入字串***: "
           << str15_1.insert(5, "***") <<endl;

    cout << "str15_1长度: " <<str15_1.length() <<endl;

    return 0;
}
