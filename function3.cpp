#include <iostream>

using namespace std;

int foo();

int main()
{
    int (*ptr)() = 0;
    
    ptr = foo; 

    foo(); 
    ptr(); 

    cout << "address of foo:" 
         << (int)foo << endl; 
    cout << "address of foo:" 
         << (int)ptr << endl; 

    return 0; 
}

int foo()
{
    cout << "function point" << endl;

    return 0;
}
