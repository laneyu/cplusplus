#include <iostream>
#include <boost/noncopyable.hpp>

class A : public boost::noncopyable
{
public:
	A()
	{
		std::cout <<"defanged!" << std::endl;
	}
};

int main()
{
	A obj1;
//	A obj2(obj1);
	A obj3;
	
//	obj3 = obj1;
}
