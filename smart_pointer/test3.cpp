//http://www.cplusplus.com/reference/memory/shared_ptr/shared_ptr/

#include <iostream>
#include <memory>
#include <boost/shared_ptr.hpp>

#include <string.h> //memcpy
#include <assert.h>

int main()
{
	boost::shared_ptr<int> p1;
	assert(p1.use_count() == 0);
	assert(p1.get() == 0);
	
	boost::shared_ptr<int> p3(new int);
	assert(p3.use_count() == 1);
	
//	boost::shared_ptr<int> p4(new int, std::default_delete<int>());
//	assert(p4.use_count() == 1);
	
//	boost::shared_ptr<int> p5(new int, [](int* p){delete p;}, std::allocator<int>());
	
	boost::shared_ptr<int> p3_1(p3);
	assert(p3_1.get() == p3.get());
	assert(p3_1.use_count() == p3.use_count());
	assert(p3_1.use_count() == 2);
	
	std::cout <<"All Pass!\n";
}