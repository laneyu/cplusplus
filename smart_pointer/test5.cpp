#include <iostream>
#include <boost/scoped_ptr.hpp>

int main()
{
	boost::scoped_ptr<int> p1(new int);
	boost::scoped_ptr<int> p2(new int);
	*p1 = 1;
	*p2 = 2;
	
	std::cout <<"p1:" <<p1.get()<<" " <<*p1 <<"\n";
	std::cout <<"p2:" <<p2.get()<<" " <<*p2 <<"\n";
	
	std::cout<<"\nswap\n";
	p1.swap(p2);
	std::cout <<"p1:" <<p1.get()<<" " <<*p1 <<"\n";
	std::cout <<"p2:" <<p2.get()<<" " <<*p2 <<"\n";
}

//result
//p1:0x936e008 1
//p2:0x936e018 2
//
//swap
//p1:0x936e018 2
//p2:0x936e008 1