#include <iostream>
#include <boost/shared_ptr.hpp>
#include <boost/enable_shared_from_this.hpp>

#include <string.h> //memcpy
#include <assert.h>

class Y : public boost::enable_shared_from_this<Y>
{
public:
	boost::shared_ptr<Y> f()
	{
		return shared_from_this();
	}
}; 

void deleter()
{
		std::cout <<"deleter\n";
}

//BOOST VERSION 1.49
void test_basic_usage()
{
	std::cout <<__FUNCTION__ <<"\n";
	
	//deafult constructor 
	//compile failed
//	boost::shared_ptrp p1;
//	assert(p1.use_count() == 0);
//	assert(p1.get() == 0);
	
	//pointer constructor
	int *p2_1 = new int;
	boost::shared_ptr<void> p2_2(p2_1);
	assert(p2_2.get() == p2_1);
	assert(p2_2.use_count() == 1);
	
	//enable_shared_from_this
	boost::shared_ptr<Y> p3(new Y);
	boost::shared_ptr<Y> p3_1 = p3->f();
	assert(p3.use_count() == 2);
	assert(p3 == p3_1);
	
}

int main()
{
	test_basic_usage();
	
	std::cout <<"All Pass!\n";
}