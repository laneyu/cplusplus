#include <iostream>
#include <boost/shared_ptr.hpp>

#include <string.h> //memcpy
#include <assert.h>

void test_single_value()
{
	std::cout <<__FUNCTION__ <<"\n";
	//int
	boost::shared_ptr<int> p1( new int );
	
	//double
	boost::shared_ptr<double> p2 ( new double );
	
	//char
	boost::shared_ptr<char> p3 ( new char );
	
	*p1 = 10;
	*p2 = 128.051;
	*p3 = 'A';
	
	std::cout <<"p1:" <<p1 <<"-->" <<*p1 <<"\n";
	std::cout <<"p2:" <<p2 <<"-->" <<*p2 <<"\n";
	std::cout <<"p3:" <<p3 <<"-->" <<*p3 <<"\n";
}

void test_array()
{
	std::cout <<__FUNCTION__ <<"\n";
	
	const int kTmpBufSize = 1024;
	
	//int
	int tmpInt[kTmpBufSize];
	memset(tmpInt, 0x55, sizeof(tmpInt));
	boost::shared_ptr<int> p1(new int[kTmpBufSize]);
	memcpy(p1.get(), tmpInt, kTmpBufSize*sizeof(int));
	assert(memcmp(p1.get(), tmpInt, kTmpBufSize*sizeof(int)) == 0);
	
	//double
	double tmpDouble[kTmpBufSize];
	for (int i = 0; i < kTmpBufSize; ++i)
	{
		tmpDouble[i] = 123456.7890;
	}
	boost::shared_ptr<double> p2(new double[kTmpBufSize]);
	memcpy(p2.get(), tmpDouble, kTmpBufSize*sizeof(double));
	assert(memcmp(p2.get(), tmpDouble, kTmpBufSize*sizeof(double)) == 0);
	
	//char
	char tmpChar[kTmpBufSize];
	memset(tmpChar, 'A', kTmpBufSize*sizeof(char));
	boost::shared_ptr<char> p3(new char[kTmpBufSize]);
	memcpy(p3.get(), tmpChar, kTmpBufSize*sizeof(char));
	assert(memcmp(p3.get(), tmpChar, kTmpBufSize*sizeof(char)) == 0);
} 

int main()
{
	test_single_value();
	
	std::cout <<"\n";
	test_array();
	
	std::cout <<"\n";
	
	std::cout <<"All Pass!\n";
}